/*
 * GkDebconf -- Help to configure packages with debconf
 * Copyleft (C) 2003 Agney Lopes Roth Ferraz <agney@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <gtk/gtk.h>

#include "../config.h"
#include "defines.h"

#include "files.h"
#include "interface.h"

/*
 * A pointer to the main window.
 */
GtkWidget *main_window;

/**
 * main:
 * @argc: Array with parameters given to gkdebconf
 * @argv: number of parameters given to gkdebconf
 *
 * This function is the first called funcion when gkdebconf starts.
 * Returns: 1 if exit normally, another number otherwise
 */

int
main (int argc, char **argv)
{
  GtkWidget *splashwin;

  /*
   * Gettext stuff
   */
  bindtextdomain (PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (PACKAGE, "UTF-8");
  textdomain (PACKAGE);

  gtk_init (&argc, &argv);

  /*
   * shows a splash screen
   */
  splashwin = gk_splash ();
  gtk_widget_show_now (splashwin);

  while (gtk_events_pending())
    gtk_main_iteration();

  /*
   * creates main window check the gtk signal bindings done in this
   * function to know what functions are calledto detect the packages
   * that are configurable    and sort them
   */

  main_window = create_main_window();

  /*
   * destroy splash and show main
   */
  gtk_widget_destroy (splashwin);
  gtk_widget_show (main_window);

  /*
   *	let's use the program
   */
  gtk_main();
  return 0;
}

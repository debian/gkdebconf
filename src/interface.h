/*
 * GkDebconf -- Help to configure packages with debconf
 * Copyleft (C) 2003 Agney Lopes Roth Ferraz <agney@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GTK_GUI_H_

GtkWidget* create_main_window (void);
GtkWidget* create_menubar (GtkWidget *win);
GtkWidget* create_gkdebconf_image (gchar *iname);
void show_about_window (GtkWidget *main_window, gchar *msg, gchar *iname);
void gk_dialog (GtkMessageType type, gchar *msg, ...);

void set_description(GtkWidget *text, gchar *pkg);
void gtk_list_store_set_strings (GtkListStore *store, GList *cflist);
void list_item_selected(GtkTreeSelection *selection, gpointer data);
void cf_select(GtkTreeSelection *selection, gpointer data);
GtkWidget* gk_splash (void);
void about_cb (GtkWidget *w, gpointer data);
void gk_dialog_for_gconf (  gchar *key, gchar *format, ... );
#endif

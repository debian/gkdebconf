/*
 * GkDebconf -- Help to configure packages with debconf
 * Copyleft (C) 2003 Agney Lopes Roth Ferraz <agney@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#include <gtk/gtk.h>

#include "../config.h"

#include "defines.h"
#include "config.h"

#define SCHEMA "org.debian.gkdebconf"

static GSettings *settings = NULL;

/**
 * write_config:
 *
 * @option: gconf option in gkdebconf registry.
 * @data: the value of option.
 *
 *  Writes and gconf2 registry on gkdebconf registry.
 * Returns: 0 on succes and 1 on error.
 */

int
write_config (gchar *option, gchar *data)
{
  if (!settings)
    settings = g_settings_new (SCHEMA);

  return !g_settings_set_string (settings, option, data);
}

/**
 * read_config:
 *
 * @option: gconf key in gkdebconf registry.
 *
 *  Reads and gconf2 key on gkdebconf registry.
 *
 * Returns: The key (option) value. ¨none¨ if failed.
 */

char*
read_config (gchar *option)
{
  gchar *data = NULL;

  if (!settings)
    settings = g_settings_new (SCHEMA);

  data = g_settings_get_string (settings, option);

  return data;
}

/**
 * remember_fe_cb:
 *
 * @data: data in w.
 * @action: the event that happens on w.
 * @w: widget that will be checked if is active or not.
 *
 *  Update remember_frontend key on gkdebconf gconf2 registry.
 */

void
remember_fe_cb (gpointer data, guint action, GtkWidget *w )
{
  if (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (w)))
    write_config ("remember-frontend", "yes");
  else
    write_config ("remember-frontend", "no");
}

/**
 * show_libgnome_missing_alert:
 *
 * @data: data in w.
 * @action: the event that happens on w.
 * @w: widget that will be checked if is active or not.
 *
 *  Update libgnome_alert key on gkdebconf gconf2 registry.
 */

void
show_libgnome_missing_alert (gpointer data, guint action, GtkWidget *w )
{
  if (gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (w)))
    write_config ("libgnome-alert", "yes");
  else
    write_config ("libgnome-alert", "no");
}
